import * as express from 'express'
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';

import PqrsRouter from './routes/prueba.router';
import { environment } from './environment';

const version = environment.api_version;

class App {
    public express: express.Application;

    constructor() {
        this.express = express();
        this.middleware();
        this.enableCors(); 
        this.mountRoutes();
    }

    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json({limit: '50mb', extended: true}));
        this.express.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
    }

    // Configure API endpoints.
    private mountRoutes(): void {
        this.express.use(`${version}`, PqrsRouter);
    }

    private enableCors() {
        this.express.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-api-key");
        res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE,PATCH");
        res.header("Access-Control-Allow-Credentials", "true");
        next();
        });
    }
}

export default new App().express
