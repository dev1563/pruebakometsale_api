import app from './App'
import * as https from 'https';
import * as http from 'http';
import * as fs from 'fs';

var privateKey  = fs.readFileSync(process.env.SSLKEY  || './certs/server.key', 'utf8');
var certificate = fs.readFileSync(process.env.SSLCERT || './certs/server.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate , passphrase:"admin"};

let portSsl = process.env.SSLPORT || 9443;
let serverSsl = https.createServer(credentials, app);
console.info(`server is listening on secure port ${portSsl}`)
serverSsl.on('error', onError);
serverSsl.listen(portSsl, (err) => {
  if (err) {
    return console.info(err)
  }
  return console.info(`server is listening on secure port ${portSsl}`)
});

let port = process.env.PORT || 8000;
let server = http.createServer(app);
server.on('error', onError);
server.listen(port, (err) => {
  if (err) {
    return console.error(err)
  }
  return console.info(`server is listening on ${port}`)
});

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  console.error(error);
  if (error.syscall !== 'listen') {
      throw error;
  }

  let bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
      case 'EACCES':
          console.error(bind + ' requires elevated privileges');
          process.exit(1);
          break;
      case 'EADDRINUSE':
          console.error(bind + ' is already in use');
          process.exit(1);
          break;
      default:
          throw error;
  }
}
