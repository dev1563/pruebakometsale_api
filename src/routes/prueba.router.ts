import {Router, Request, Response, NextFunction} from 'express';
import { environment } from '../environment';
import { RouterBase } from './RouterBase';
import Logger from '@landsoftco/customlogger';

const api_base: string = environment.pqrs_base_endpoint;
const logger = new Logger(environment.production)

export class PruebaRouter extends RouterBase {
    router: Router;

    /**
     * Initialize the Router
     */
    constructor() {
        super();
        this.router = Router();
        this.init();
    }

    /**
     * POST command to micro app
     */
    public postCommand(req: Request, res: Response, next: NextFunction) {
        super.postCommandToBackend(api_base, req, res);
    }

    public getHealthCheck(req: Request, res: Response, _next: NextFunction) {
        logger.info('message')
        logger.error('message')
        logger.debug(`message with nex info ${JSON.stringify({ 'INFO_MESSAGE': 'Esto es un mensaje con datos json modificado' })}`)
    
        res.status(200).send("OK");
    }

    public saveFlowers(req: Request, res: Response, next: NextFunction) {
        super.postControllerToBackend(api_base, '/saveFlower', req, res);
    }

    public getListFlowers(req: Request, res: Response, next: NextFunction) {
        super.queryBackend(api_base, '/listSaved', req, res);
    }

    public getListFlowersToPrice(req: Request, res: Response, next: NextFunction) {
        super.queryBackend(api_base, '/listPrice', req, res);
    }

    public getListFlowersToName(req: Request, res: Response, next: NextFunction) {
        super.queryBackend(api_base, `/listName/${req.params.name}`, req, res);
    }

    public daleteFlower(req: Request, res: Response, next: NextFunction) {
        super.deleteToBackend(api_base, `/delete/${req.params.id}`, req, res);
    }

    public updateFlower(req: Request, res: Response, next: NextFunction) {
        super.putToBackend(api_base,'/updateFlower', req, res);
    }

    

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/health-check', this.getHealthCheck);
        this.router.post('/saveFlower', this.saveFlowers);
        this.router.get('/listSaved', this.getListFlowers);
        this.router.get('/listPrice', this.getListFlowersToPrice);
        this.router.get('/listName/:name', this.getListFlowersToName);
        this.router.delete('/delete/:id', this.daleteFlower);
        this.router.put('/updateFlower', this.updateFlower);
    }

}

// Create the Router, and export its configured Express.Router
const pruebaRouter = new PruebaRouter();
pruebaRouter.init();

export default pruebaRouter.router;
