import * as unirest from 'unirest';
import { Request, Response } from 'express';
import * as _ from 'lodash';

/**
 * Esta clase ofrece metodos base para usar en los Routers.
 * 
 * Cada router debe heredar de esta clase. (Ver admin.router.ts para un ejemplo)
 */
export abstract class RouterBase {

    /**
     * Metodo base para invocar queries (GETS) al backend.
     * 
     * @param base el base del API (corresponde al URI de la microaplicacion java)
     * @param uri resto de la URL especifica
     * @param req objeto request
     * @param res objeto response
     * @param headers un array de objetos: { name:'nombreHeader', value:'valorHeader' }.
     */
    protected queryBackend(base:String, uri: string, req: Request, res: Response, headers?: any) {

        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }

        if (headers && _.isArray(headers)) {
            _.foreach(headers, e=> {
                headersObj[e.name] = e.value;
            })
        }

        unirest.get(base + uri)
            .headers(headersObj)
            .query(req.query || '') //agrega el query original si es que existe.
            .end(function (response) {
                if (response.error) {   
                    if (response.body) {
                        console.error(`STATUS=ERROR, PROCESO=, ID_COMANDO=, TOKEN=${req.params.pqrsId} - ${req.params.lastName}, EVENTO=${uri}, CODE_HTTP=${response.statusCode}, CODE_ERROR=${response.status}, CALL=MICROSERVICE, MSG=${response.error}, RESPONSE=${JSON.stringify(response.body)}`);
                    } else{
                        console.error(`STATUS=ERROR, PROCESO=, ID_COMANDO=, TOKEN=${req.params.pqrsId} - ${req.params.lastName}, EVENTO=${uri}, CODE_HTTP=${response.error.code}, CODE_ERROR=${response.error.code}, CALL=NOMICROSERVICE, MSG=${response.error}, RESPONSE=${JSON.stringify(response)}`);
                    }
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    console.log(`STATUS=OK, PROCESO=, ID_COMANDO=, TOKEN=${req.params.pqrsId} - ${req.params.lastName}, EVENTO=${uri}, CODE_HTTP=${response.status}, CODE_ERROR=, CALL=MICROSERVICE, MSG="OK", RESPONSE=`);
                    res.status(response.status || 200).send({
                        response: response.body
                    });
                }
            });
    }

    /**
     * Metodo base para enviar comandos (POST) al backend.
     * 
     * @param base el base del API (corresponde al URI de la microaplicacion java). El metodo va a agregar el uri '/commands'
     * @param req objeto request
     * @param res objeto response
     * @param headers un array de objetos: { name:'nombreHeader', value:'valorHeader' }.
     */
    protected postCommandToBackend(base:String, req: Request, res: Response, headers?: any) {

        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }
        if (headers && _.isArray(headers)) {
            _.foreach(headers, e=> {
                headersObj[e.name] = e.value;
            })
        }

        unirest.post(base + '/commands')
            .headers(headersObj)
            .send(req.body)
            .end(function (response) {
                if (response.error) {
                    if (response.body) {
                        console.error(`STATUS=ERROR, PROCESO=PAGO, ID_COMANDO=${req.body.comando.nombre}, TOKEN=${req.body.comando.payload.securityToken}, EVENTO=/commands, AGENTE=${req.body.comando.payload.userAgent},CODE_HTTP=${response.statusCode}, CODE_ERROR=${response.body.code}, CALL=MICROSERVICE, MSG=${response.body.code} - ${response.body.message}, RESPONSE=${JSON.stringify(response.body)}`);
                    } else{
                        console.error(`STATUS=ERROR, PROCESO=PAGO, ID_COMANDO=${req.body.comando.nombre},  TOKEN=${req.body.comando.payload.securityToken}, EVENTO=/commands, AGENTE=${req.body.comando.payload.userAgent}, EVENTO=/commands, CODE_HTTP=${response.error.code}, CODE_ERROR=${response.error.code}, CALL=NOMICROSERVICE, MSG=${response.error}, RESPONSE=${JSON.stringify(response)}`);
                    }
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    console.log(`STATUS=OK, PROCESO=PAGO, ID_COMANDO=${req.body.comando.nombre}, TOKEN=${req.body.comando.payload.securityToken}, EVENTO=/commands, AGENTE=${req.body.comando.payload.userAgent}, EVENTO=/commands, CODE_HTTP=${response.status}, CODE_ERROR=, CALL=MICROSERVICE, MSG="OK", RESPONSE=`);
                    res.status(response.status || 202).send({
                        response: response.body
                    });
                }
            });
    }

    protected postControllerToBackend(base:String, uri: string, req: Request, res: Response) {
        const header = req.headers;
        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json', ...header};
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }
        const start = Date.now();
        
            unirest.post(base + uri)
            .headers(headersObj)
            .send(req.body)
            .end(function (response) {
                if (response.error) {
                    if (response.body) {
                        console.log(`STATUS=ERROR, METHOD=POST, API=ADMIN, EVENTO=${uri}, TOKEN=${req.headers['token-user'] || 'N/A' }, PATH=${req.headers['path-site']}, ORIGIN_SITE=${req.headers['origin']}, TIME=${((Date.now() - start )/1000)}, CODE_HTTP=${response.statusCode}, CODE_ERROR=${response.body.code || 'N/A'}, CALL=MICROSERVICE, MSG=${response.body.code || 'N/A'} - ${response.body.message || 'N/A'}, BODY=${JSON.stringify(req.body)}, RESPONSE=${JSON.stringify(response.body)}`);
                    } else{
                        console.log(`STATUS=ERROR, METHOD=POST, API=ADMIN, EVENTO=${uri}, TOKEN=${req.headers['token-user'] || 'N/A' }, PATH=${req.headers['path-site']}, ORIGIN_SITE=${req.headers['origin']}, TIME=${((Date.now() - start )/1000)}, CODE_HTTP=${response.error.code || 'N/A'}, CODE_ERROR=${response.error.code || 'N/A'}, CALL=NOMICROSERVICE, MSG=${response.error}, BODY=${JSON.stringify(req.body)}, RESPONSE=${JSON.stringify(response)}`);
                    }
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    console.log(`STATUS=OK, METHOD=POST, API=ADMIN, EVENTO=${uri}, TOKEN=${req.headers['token-user'] || 'N/A' }, PATH=${req.headers['path-site']}, ORIGIN_SITE=${req.headers['origin']}, TIME=${((Date.now() - start )/1000)}, CODE_HTTP=${response.status}, CODE_ERROR=, CALL=MICROSERVICE, MSG="OK", RESPONSE=`);
                    res.status(response.status || 202).send({
                        response: response.body
                    });
                }
            });
       
    }
    

    protected postToBackend(base:String, uri: string, req: Request, res: Response, headers?: any) {
        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }
        if (headers && _.isArray(headers)) {
            _.foreach(headers, e=> {
                headersObj[e.name] = e.value;
            })
        }

        unirest.post(base + uri)
            .headers(headersObj)
            .query(req.query || '') //agrega el query original si es que existe.
            .send(req.body)
            .end(function (response) {
                if (response.error) {
                    console.log(`Error Code: ${response.status}, Msg: ${response.error}`);
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    res.status(response.status || 202).send({
                        response: response.body
                    });
                }
            });
    }

    protected putToBackend(base: String, uri: string, req: Request, res: Response, headers?: any){
        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }
        if (headers && _.isArray(headers)) {
            _.foreach(headers, e => {
                headersObj[e.name] = e.value;
            })
        }

        unirest.put(base + uri)
            .headers(headersObj)
            .send(req.body)
            .end(function (response) {
                if (response.error) {
                    console.log(`Error Code: ${response.status}, Msg: ${response.error}`);
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    res.status(response.status || 202).send({
                        response: response.body
                    });
                }
            });
    }

    protected deleteToBackend(base: String, uri: string, req: Request, res: Response, headers?: any) {
        let headersObj = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        if (req.get('Authorization') != undefined) {
            headersObj['Authorization'] = req.get('Authorization');
        }
        if (headers && _.isArray(headers)) {
            _.foreach(headers, e => {
                headersObj[e.name] = e.value;
            })
        }

        unirest.delete(base + uri)
            .headers(headersObj)
            .send(req.body)
            .end(function (response) {
                if (response.error) {
                    console.log(`Error Code: ${response.status}, Msg: ${response.error}`);
                    res.status(response.status || 500).send({
                        error: response.error
                    });
                } else {
                    res.status(response.status || 202).send({
                        response: response.body
                    });
                }
            });
    }
}