export const environment = {
	production: false,
	api_version: '/v1',
	pqrs_base_endpoint: 'http://localhost:8443',
};
