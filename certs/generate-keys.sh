
openssl genrsa -out server.key 1024

openssl req -new -out server.csr -key server.key -config openssl.cnf

openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt -extensions v3_req -extfile openssl.cnf

rm server.csr